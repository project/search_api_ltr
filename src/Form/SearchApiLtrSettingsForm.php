<?php

namespace Drupal\search_api_ltr\Form;

use Drupal\Component\Utility\Html;
use Drupal\Core\Access\AccessResult;
use Drupal\Core\Entity\EntityTypeManagerInterface;
use Drupal\Core\Form\FormBase;
use Drupal\Core\Form\FormStateInterface;
use Drupal\search_api\IndexInterface;
use Drupal\search_api_ltr\Entity\SearchApiLtr;
use Drupal\search_api_ltr\LtrTrainerPluginManager;
use Drupal\search_api_solr\Plugin\search_api\backend\SearchApiSolrBackend;
use Symfony\Component\DependencyInjection\ContainerInterface;

/**
 * Class SearchApiLtrSettingsForm.
 */
class SearchApiLtrSettingsForm extends FormBase {

  /**
   * Ltr Trainer plugin Manager.
   *
   * @var \Drupal\search_api_ltr\LtrTrainerPluginManager
   */
  private $ltrTrainerPluginManager;

  /**
   * Entity type manager service.
   *
   * @var \Drupal\Core\Entity\EntityTypeManagerInterface
   */
  protected $entityTypeManager;

  /**
   * Search Api Ltr Config Entity Manager.
   *
   * @var \Drupal\Core\Entity\EntityStorageInterface
   */
  protected $searchApiLtrStorage;

  /**
   * The index these search api ltr settings apply to.
   *
   * @var \Drupal\search_api\IndexInterface
   */
  protected $index;

  /**
   * The index these search api ltr settings apply to.
   *
   * @var \Drupal\search_api\ServerInterface
   */
  protected $server;

  /**
   * {@inheritdoc}
   */
  public function __construct(LtrTrainerPluginManager $ltr_trainer_plugin_manager, EntityTypeManagerInterface $entity_type_manager) {
    $this->ltrTrainerPluginManager = $ltr_trainer_plugin_manager;
    $this->entityTypeManager = $entity_type_manager;
    $this->searchApiLtrStorage = $this->entityTypeManager->getStorage('search_api_ltr');

  }

  /**
   * {@inheritdoc}
   */
  public static function create(ContainerInterface $container) {
    return new static(
      $container->get('plugin.manager.search_api_ltr.ltr_trainer'), $container->get('entity_type.manager')
    );
  }

  /**
   * Returns a unique string identifying the form.
   *
   * @return string
   *   The unique string identifying the form.
   */
  public function getFormId() {
    return 'search_api_ltr';
  }

  /**
   * {@inheritdoc}
   */
  public function buildForm(array $form, FormStateInterface $form_state, IndexInterface $search_api_index = NULL) {
    $this->index = $search_api_index;
    $this->server = $search_api_index->getServerInstance();
    // Check if we support this feature.
    if (!$this->server->supportsFeature('search_api_ltr')) {
      return [];
    }
    $this->config = $this->searchApiLtrStorage->load($this->index->id());

    // If it doesn't exist, create it already.
    if (empty($this->config)) {
      $this->config = SearchApiLtr::create();
      $this->config->setIndexId($this->index->id());
      $this->config->setId($this->index->id());
    }

    // See if the backend implemented
    // \Drupal\search_api_ltr\LtrBackendInterface.
    $models = [];
    if (method_exists($this->server, 'getLtrModels')) {
      $models = $this->server->getLtrModels();
    }
    // Remove this as soon as this was added to Search Api Solr.
    elseif ($this->server->getBackend() instanceof SearchApiSolrBackend) {
      // Get Solr Backend.
      /** @var \Drupal\search_api_solr\Plugin\search_api\backend\SearchApiSolrBackend $solrBackend */
      $solrBackend = $this->index->getServerInstance()->getBackend();

      // Check if Solr is available, do not show anything if it isn't.
      if ($solrBackend->isAvailable()) {
        $response = $solrBackend->getSolrConnector()->coreRestGet('schema/model-store');
        if (!empty($response['models'])) {
          foreach ($response['models'] as $model) {
            $models[] = $model['name'];
          }
        }
      }
    }

    // Assume we have our models now, either by support of our own or by the
    // backend itself.
    $options = [];
    foreach ($models as $model) {
      $options[$model] = $model;
    }

    $form['search_api_ltr']['#markup'] = 'Settings form for Learn to Rank. Manage configuration here.';

    // Define a per index setting.
    // Model Selector.
    $form['model'] = [
      '#type' => 'select',
      '#title' => $this->t('Learn To Rank Model'),
      '#options' => $options,
      // Retrieve the per index setting.
      '#default_value' => $this->config->getModel(),
      '#description' => $this->t('The model to use for this index'),
      '#required' => TRUE,
    ];

    // Amount of documents to rerank.
    $form['documents'] = [
      '#type' => 'number',
      '#title' => $this->t('How many documents to rerank?'),
      // Retrieve the per index setting.
      '#default_value' => $this->config->getDocs(),
      '#description' => $this->t('The amount of documents to rerank'),
      '#required' => TRUE,
      '#min' => 1,
      '#step' => 1,
    ];

    $form['ltr_trainer_method'] = [
      '#type' => 'select',
      '#title' => $this->t('Ltr Trainer method'),
      '#description' => $this->t('Select the Ltr trainer method you want to use.'),
      '#empty_value' => '',
      '#options' => $this->getLtrTrainerPluginInformations()['labels'],
      '#default_value' => $this->config->getLtrTrainerMethod(),
      '#required' => TRUE,
      '#ajax' => [
        'callback' => [get_class($this), 'buildAjaxLtrTrainerConfigForm'],
        'wrapper' => 'search-api-ltr-ltr-trainer-config-form',
        'method' => 'replace',
        'effect' => 'fade',
      ],
    ];

    $this->buildLtrTrainerConfigForm($form, $form_state);

    // Amount of documents to rerank.
    $form['relevancyAnnotations'] = [
      '#type' => 'textarea',
      '#title' => $this->t('Relevancy Annotations'),
      '#description' => $this->t('The json representation of the training data for the LTR model.'),
      '#default_value' => $this->config->getRelevancyAnnotations(),
    ];

    $form['config_entity_id'] = [
      '#type' => 'hidden',
      '#value' => $this->config->id(),
    ];

    $form['save'] = [
      '#type' => 'submit',
      '#value' => $this->t('Save'),
    ];
    return $form;
  }

  /**
   * {@inheritdoc}
   */
  public function validateForm(array &$form, FormStateInterface $form_state) {

    // If it is from the configuration.
    $ltr_trainer_plugin_id = $form_state->getValue('ltr_trainer_method');
    if ($ltr_trainer_plugin_id) {
      $configuration = $this->config->get($ltr_trainer_plugin_id . '_configuration');
      if (empty($configuration)) {
        $configuration = [];
      }
      $ltr_trainer_plugin = $this->getLtrTrainerPluginManager()->createInstance($ltr_trainer_plugin_id, $configuration);

      // Validate the ltr_trainer_config part of the form only if it
      // corresponds to the current $ltr_trainer_plugin_id.
      if (!empty($form['ltr_trainer_config']['ltr_trainer_method']['#value']) && $form['ltr_trainer_config']['ltr_trainer_method']['#value'] == $ltr_trainer_plugin_id) {
        $ltr_trainer_plugin->validateConfigurationForm($form, $form_state);
      }
    }
  }

  /**
   * Form submission handler.
   *
   * @param array $form
   *   An associative array containing the structure of the form.
   * @param \Drupal\Core\Form\FormStateInterface $form_state
   *   The current state of the form.
   *
   * @throws \Exception
   */
  public function submitForm(array &$form, FormStateInterface $form_state) {
    // It is due to the ajax.
    $ltr_trainer_plugin_id = $form_state->getValue('ltr_trainer_method');
    if ($ltr_trainer_plugin_id) {
      $configuration = $this->config->get($ltr_trainer_plugin_id . '_configuration');
      if (empty($configuration)) {
        $configuration = [];
      }
      $ltr_trainer_plugin = $this->getLtrTrainerPluginManager()->createInstance($ltr_trainer_plugin_id, $configuration);
      $ltr_trainer_plugin->submitConfigurationForm($form, $form_state);
    }

    // Fetch the configuration object again after the plugin modified it.
    if (isset($form['config'])) {
      $this->config = $form['config'];
    }

    // Set the ltr trainer method variable.
    $this->config->set('ltrTrainerMethod', $ltr_trainer_plugin_id);
    $this->config->set('indexId', $this->index->id());
    $this->config->set('model', $form_state->getValue('model'));
    $this->config->set('docs', $form_state->getValue('documents'));
    $this->config->set('relevancyAnnotations', $form_state->getValue('relevancyAnnotations'));
    $this->config->save();

    // @todo add this through dep injection.
    $messenger = \Drupal::messenger();
    $messenger->addMessage($this->t('Configuration was saved.'));

    $form_state->setRebuild();
  }

  /**
   * Get definition of Ltr Trainer plugins from their annotation definition.
   *
   * @return array
   *   Array with 'labels' and 'descriptions' as keys containing plugin ids
   *   and their labels or descriptions.
   */
  public function getLtrTrainerPluginInformations() {
    $options = [
      'labels' => [],
      'descriptions' => [],
    ];
    foreach ($this->getLtrTrainerPluginManager()->getDefinitions() as $plugin_id => $plugin_definition) {
      $options['labels'][$plugin_id] = Html::escape($plugin_definition['label']);
      $options['descriptions'][$plugin_id] = Html::escape($plugin_definition['description']);
    }
    return $options;
  }

  /**
   * Subform.
   *
   * It will be updated with Ajax to display the configuration of an
   * ltr trainer plugin method.
   *
   * @param array $form
   *   The form array.
   * @param \Drupal\Core\Form\FormStateInterface $form_state
   *   The form state object.
   */
  public function buildLtrTrainerConfigForm(array &$form, FormStateInterface $form_state) {
    $form['ltr_trainer_config'] = [
      '#type' => 'container',
      '#attributes' => [
        'id' => 'search-api-ltr-ltr-trainer-config-form',
      ],
      '#tree' => TRUE,
    ];

    if ($form_state->getValue('ltr_trainer_method') != '') {
      // It is due to the ajax.
      $ltr_trainer_plugin_id = $form_state->getValue('ltr_trainer_method');
    }
    else {
      $ltr_trainer_plugin_id = $this->config->getLtrTrainerMethod();
      $ajax_submitted_empty_value = $form_state->getValue('form_id');
    }
    $form['ltr_trainer_config']['#type'] = 'details';
    $form['ltr_trainer_config']['#open'] = TRUE;
    // If the form is submitted with ajax and the empty value is chosen or if
    // there is no configuration yet and no ltr trainer method was chosen in the
    // form.
    if (isset($ajax_submitted_empty_value) || $ltr_trainer_plugin_id == '') {
      $form['ltr_trainer_config']['#title'] = $this->t('Please make a choice');
      $form['ltr_trainer_config']['#description'] = $this->t('Please choose an LTR Trainer method in the list above.');
    }
    else {
      $configuration = $this->config->get($ltr_trainer_plugin_id . '_configuration');
      if (empty($configuration)) {
        $configuration = [];
      }
      $ltr_trainer_plugin = $this->getLtrTrainerPluginManager()->createInstance($ltr_trainer_plugin_id, $configuration);
      $form['ltr_trainer_config']['#title'] = $this->t('@ltr_trainer_plugin_label configuration', ['@ltr_trainer_plugin_label' => $this->getLtrTrainerPluginInformations()['labels'][$ltr_trainer_plugin_id]]);
      $ltr_trainer_form = $ltr_trainer_plugin->buildConfigurationForm([], $form_state);

      $form['ltr_trainer_config']['ltr_trainer_method'] = [
        '#type' => 'value',
        '#value' => $ltr_trainer_plugin_id,
      ];
      $form['ltr_trainer_config'] += $ltr_trainer_form;
    }
  }

  /**
   * Ajax callback.
   *
   * @param array $form
   *   The form array.
   * @param \Drupal\Core\Form\FormStateInterface $form_state
   *   The form state object.
   *
   * @return array
   *   subform
   */
  public static function buildAjaxLtrTrainerConfigForm(array $form, FormStateInterface $form_state) {
    // We just need to return the relevant part of the form here.
    return $form['ltr_trainer_config'];
  }

  /**
   * Returns the ltr trainer plugin manager.
   *
   * @return \Drupal\search_api_ltr\LtrTrainerPluginManager
   *   The ltr trainer plugin manager.
   */
  protected function getLtrTrainerPluginManager() {
    return $this->ltrTrainerPluginManager ?: \Drupal::service('plugin.manager.search_api_ltr.ltr_trainer');
  }

  /**
   * Checks access for the LTR config form.
   *
   * @param \Drupal\search_api\IndexInterface $search_api_index
   *   The index for which access should be tested.
   *   For now we only support Solr, as soon as the feature is added to
   *   Search Api Solr we can remove this and get the functions from the
   *   backend.
   *
   * @return \Drupal\Core\Access\AccessResultInterface
   *   The access result.
   *
   * @throws \Exception
   */
  public function access(IndexInterface $search_api_index) {
    $search_api_server = $search_api_index->getServerInstance();

    return AccessResult::allowedIf(
      $search_api_server->hasValidBackend()
      && $search_api_server->supportsFeature('search_api_ltr')
    )->addCacheableDependency($search_api_index);
  }

}
