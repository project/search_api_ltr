<?php

namespace Drupal\search_api_ltr;

use Drupal\Core\Plugin\PluginFormInterface;
use Drupal\Component\Plugin\ConfigurableInterface;

/**
 * Provides an interface for a plugin that trains a model based on feedback.
 *
 * @ingroup plugin_api
 */
interface LtrTrainerPluginInterface extends PluginFormInterface, ConfigurableInterface {

  /**
   * Training function that uses the configuration provided and returns a model.
   *
   * @param string $data
   *   The training data.
   *
   * @return string
   *   The JSON model to upload to solr.
   */
  public function train($data);

}
