<?php

namespace Drupal\search_api_ltr\Entity;

use Drupal\Core\Config\Entity\ConfigEntityBase;

/**
 * Defines the Search api ltr index configuration entity.
 *
 * @ConfigEntityType(
 *   id = "search_api_ltr",
 *   label = @Translation("Search api ltr settings"),
 *   admin_permission = "administer search_api",
 *   config_prefix = "search_api_ltr",
 *   entity_keys = {
 *     "id" = "id"
 *   },
 *   links = {
 *     "edit-form" = "/admin/config/search/search-api/ltr/{search_api_ltr}",
 *   },
 *   config_export = {
 *     "indexId" = "indexId",
 *     "id" = "id",
 *     "model" = "model",
 *     "relevancyAnnotations" = "relevancyAnnotations",
 *     "docs" = "docs",
 *     "ltrTrainerMethod" = "ltrTrainerMethod",
 *     "ranklib_trainer_configuration" = "ranklib_trainer_configuration"
 *   }
 * )
 */
class SearchApiLtr extends ConfigEntityBase {

  /**
   * The Search Api Index Id.
   *
   * @var string
   */
  protected $indexId;

  /**
   * The Config Entity Id.
   *
   * @var string
   */
  protected $id;

  /**
   * The model that is used.
   *
   * @var string
   */
  protected $model;

  /**
   * The amount of documents to be reranked.
   *
   * @var int
   */
  protected $docs;

  /**
   * The json blob of the search result relevancy indicator.
   *
   * @var string
   */
  protected $relevancyAnnotations;

  /**
   * The training plugin id that is used.
   *
   * @var string
   */
  protected $ltrTrainerMethod;

  /**
   * The training plugin configuration.
   *
   * @var array
   */
  protected $ltrTrainerConfiguration;

  /**
   * Returns the ID of the sorts field.
   *
   * @return string
   *   The ID of the sorts field.
   */
  public function getIndexId() {
    return $this->indexId;
  }

  /**
   * Sets the index id.
   *
   * @@param string $indexId
   *   The search api index id.
   */
  public function setIndexId($indexId) {
    $this->indexId = $indexId;
  }

  /**
   * Returns the model in use.
   *
   * @return string
   *   The id of the model.
   */
  public function getModel() {
    return $this->model;
  }

  /**
   * Sets the model.
   *
   * @param string $model
   *   The model to be used.
   */
  public function setModel($model) {
    $this->model = $model;
  }

  /**
   * Returns the amount of docs to be reranked.
   *
   * @return int
   *   The amount of docs to be reranked.
   */
  public function getDocs() {
    return $this->docs;
  }

  /**
   * Sets the amount of docs to be reranked.
   *
   * @param int $docs
   *   The amount of docs to be reranked.
   */
  public function setDocs($docs) {
    $this->docs = $docs;
  }

  /**
   * Returns the blob of the relevancy annotations.
   *
   * Represented as json data blob.
   *
   * @return string
   *   The json blob.
   */
  public function getRelevancyAnnotations() {
    return $this->relevancyAnnotations;
  }

  /**
   * Set the Relevancy Annotations.
   *
   * @param string $relevancyAnnotations
   *   The json blob of the search result relevancy indicator.
   */
  public function setRelevancyAnnotations($relevancyAnnotations) {
    $this->relevancyAnnotations = $relevancyAnnotations;
  }

  /**
   * Get the LTR Trainer Method.
   *
   * @return string
   *   The LTR Trainer Method.
   */
  public function getLtrTrainerMethod() {
    return $this->ltrTrainerMethod;
  }

  /**
   * Set the LTR Trainer Method.
   *
   * @param string $ltrTrainerMethod
   *   The Trainer method used.
   */
  public function setLtrTrainerMethod(string $ltrTrainerMethod) {
    $this->ltrTrainerMethod = $ltrTrainerMethod;
  }

  /**
   * Get the Entity ID.
   *
   * @return string
   *   The Entity ID.
   */
  public function getId() {
    return $this->id;
  }

  /**
   * Set the Entity ID.
   *
   * @param string $id
   *   The Entity ID.
   */
  public function setId(string $id) {
    $this->id = $id;
  }

}
