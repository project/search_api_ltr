<?php

namespace Drupal\search_api_ltr;

use Drupal\Core\Cache\CacheBackendInterface;
use Drupal\Core\Extension\ModuleHandlerInterface;
use Drupal\Core\Plugin\DefaultPluginManager;

/**
 * Base class for ltr trainer plugin managers.
 *
 * @ingroup plugin_api
 */
class LtrTrainerPluginManager extends DefaultPluginManager {

  /**
   * {@inheritdoc}
   */
  public function __construct(\Traversable $namespaces, CacheBackendInterface $cache_backend, ModuleHandlerInterface $module_handler) {
    parent::__construct('Plugin/search_api_ltr', $namespaces, $module_handler, 'Drupal\search_api_ltr\LtrTrainerPluginInterface', 'Drupal\search_api_ltr\Annotation\SearchApiLtrLtrTrainer');
    $this->alterInfo('ltr_trainer_info');
    $this->setCacheBackend($cache_backend, 'ltr_trainer_info_plugins');
  }

}
